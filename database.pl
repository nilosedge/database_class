#!/usr/bin/perl -w

$system_dir = "system_files";
$data_dir = "data_files";

open(SYSTEM, "$system_dir/tables") or print "System files are messed up trying to recover data\n";
while(<SYSTEM>) {
	chomp;
	$system{$_} = 1;
}
close(SYSTEM);

print "\nThis following is a list of things this supports
to get the list again just type \"help\"\n\n";
$prompt = "Olin's Database>";

print $prompt; 
while(<>) {
	chomp;
	if(defined &$_){&$_;}
	else {
	if($_ =~ /PROJECT\s+([\d\w]+)\s+OVER\s+(\w+|\w+\s*(,\s*\w+\s*)+)(\s+GIVING\s+([a-zA-Z]([\d\w]?)+))?\s*$/i) { &project($1, $2, $5); }
		elsif($_ =~ /SELECT\s+([\d\w]+)\s+WHERE\s+(\w+)\s*(<|<=|=|!=|>|>=)\s*(\"[^\",]*\"|\'[^\',]*\'|\d+)(\s+GIVING\s+([a-zA-Z]([\d\w]?)+))?\s*$/i) { &select($1, $2, $3, $4, $6); }
		elsif($_ =~ /DIFFERENCE\s+([\d\w]+)\s+FROM\s+([\d\w]+)(\s+GIVING\s+([a-zA-Z]([\d\w]?)+))?\s*$/i) { &difference($1, $2, $4); }
		elsif($_ =~ /JOIN\s+([\d\w]+)\s+AND\s+([\d\w]+)\s+OVER\s+([\d\w]+)(\s+GIVING\s+([a-zA-Z]([\d\w]?)+))?\s*$/i) { &join($1, $2, $3, $5); }
		elsif($_ =~ /UNION\s+([\d\w]+)\s+AND\s+([\d\w]+)(\s+GIVING\s+([a-zA-Z]([\d\w]?)+))?\s*$/i) { &union($1, $2, $4); }
		elsif($_ =~ /INTERSECT\s+([\d\w]+)\s+AND\s+([\d\w]+)(\s+GIVING\s+([a-zA-Z]([\d\w]?)+))?\s*$/i) { &union($1, $2, $4); }
		elsif($_ =~ /TIMES\s+([\d\w]+)\s+AND\s+([\d\w]+)(\s+GIVING\s+([a-zA-Z]([\d\w]?)+))?\s*$/i) { &union($1, $2, $4); }
		elsif($_ =~ /SUM\s+([\d\w]+)\s+IN\s+([\d\w]+)\s*$/i) { &sum($1, $2); } 
		elsif($_ =~ /LIST\s+ATTRIBUTES\s+IN\s+([\d\w]+)\s*$/i) { &list($1); } 
		elsif($_ =~ /PRINT\s+([\d\w]+)\s*$/i) { &print($1); }
		elsif($_ =~ /DELETE\s+([\d\w]+)\s*$/i) { &delete($1); }
		elsif($_ =~ /COUNT\s+([\d\w]+)\s*$/i) { &count($1); }
		elsif($_ =~ /LIST\s+RELATIONS\s*$/i) { &listtables(); }
		else { print "Your query: $_\nHas errors.\n"; }
	}
	print $prompt; 
}

sub project {
		my $inT = shift @_;

	if(&valid_table($inT)) {
		my $stuff = shift @_;
		my $ouT = shift @_; 
		$stuff =~ s/\s//g;
		@attribs = split /,/, $stuff; 
		foreach $attrib (@attribs) { 
			if((!attribexists($inT, $attrib))) { print "attrib $attrib does not exist in table $inT\n"; return; }
		}
		printheader(join "\t", @attribs);
		open(DATAFILE, "< $data_dir/$inT.data");
		while(my $line = <DATAFILE>) {
			chomp $line;
			printstuff((join "\t", @attribs), $line, getlist($inT, "an"));
		} close(DATAFILE);

		if($ouT) { print "This is our outtable: $ouT\n"; }
	} else { print "That table $inT does not exist\n"; }
}

sub select {
	$inT = shift @_;
	print "This is the in table: $inT\n";
	$attrib = shift @_;
	print "This is the attrib: $attrib\n";
	$oper = shift @_;
	print "This is the oper: $oper\n";
	$const = shift @_;
	$ouT = shift @_;
	if($ouT) { print "This is our out table: $ouT\n"; }
	if($const =~ /^\'|\".*\'|\"$/) { $type = "char"; }
	else { $type = "int"; }
	print "This is the type of const $type\n";
	$const =~ s/^\'|\'$|^\"|\"$//g;
	print "This is the const: >$const<\n";
}

sub difference {
	$inT = shift @_;
	print "This is the in table: $inT\n";
	$otherT = shift @_;
	print "This is the other table: $otherT\n";
	$ouT = shift @_;
	if($ouT) { print "This is our out table: $ouT\n"; }
}

sub join {
	$inT = shift @_;
	print "This is the in table: $inT\n";
	$otherT = shift @_;
	print "This is the other table: $otherT\n";
	$attrib = shift @_;
	print "This is our attrib: $attrib\n";
	$ouT = shift @_;
	if($ouT) { print "This is our out table: $ouT\n"; }
}

sub union {
	$inT = shift @_;
	print "This is the in table: $inT\n";
	$otherT = shift @_;
	print "This is the other table: $otherT\n";
	$ouT = shift @_;
	if($ouT) { print "This is our out table: $ouT\n"; }
}

sub intersect {
	$inT = shift @_;
	print "This is the in table: $inT\n";
	$otherT = shift @_;
	print "This is the other table: $otherT\n";
	$ouT = shift @_;
	if($ouT) { print "This is our out table: $ouT\n"; }
}

sub times {
	$inT = shift @_;
	print "This is the in table: $inT\n";
	$otherT = shift @_;
	print "This is the other table: $otherT\n";
	$ouT = shift @_;
	if($ouT) { print "This is our out table: $ouT\n"; }
}

sub sum {
	$attrib = shift @_;
	print "This is the attrib: $attrib\n";
	$inT = shift @_;
	print "This is the in table: $inT\n";
}
sub list {
	if(&valid_table($_[0])) {
		print "Table $_[0]: \n\n";
		printheader(getlist($_[0], "an"));
		printheader(getlist($_[0], "at"));
		print "\n";
	} else { print "That table $_[0] does not exist\n"; }
}
sub getlist {
		open(METATABLE, "$data_dir/$_[0].meta");
		my $name = <METATABLE>;
		my $type = <METATABLE>;
		close(METATABLE);
		chomp $name;
		chomp $type;
	if($_[1] eq "an") { return $name; }
	elsif($_[1] eq "at") { return $type; }
	else { print "That was a bad type $_[1] please try something else\n"; }
}

sub attribexists {
	my $attrib = $_[1];
	@attrib_name = split /\t/, getlist($_[0], "an");
	foreach $thing (@attrib_name) { if($thing eq $attrib) { return 1; } }
	return 0;
}

sub printheader {
	my @attribs = split /\t/, $_[0];
	$counter = 0;
	foreach $attrib (@attribs) {
		print "$attrib";
		if($counter <= $#attribs) { print "\t"; }
		$counter++;
	} print "\n---------------------------------------------\n";
}

sub printstuff {
	#print "Stuff, $_[0], $_[1], $_[2]\n";
	my @attribs = split /\t/, $_[0];
	my @values = split /\t/, $_[1];
	my @attrib_list = split /\t/, $_[2];
	my $outtable = $_[3];

	if($outtable) {
		print "We are saving to the table $outtable\n";
	} else {
		for($i = 0; $i < @values; $i++) {
			$hash{$attrib_list[$i]} = $values[$i];	
		} $counter = 0;
		foreach $attrib (@attribs) {
			print "$hash{$attrib}";
			if($counter <= $#attribs) { print "\t"; }
			$counter++;
		} print "\n";
	}
}

sub print {
	my $inT = $_[0];
	if(&valid_table($inT)) {
		print "Table $inT: \n\n";
		printheader(getlist($inT, "an"));
		printheader(getlist($inT, "at"));

		open(DATAFILE, "< $data_dir/$inT.data");
		while($line = <DATAFILE>) {
			chomp $line;
			printstuff(getlist($inT, "an"), $line, getlist($inT, "an"));
		} close(DATAFILE);

		print "\n";
	} else { print "That table $inT does not exist\n"; }
}
sub delete {
	$inT = shift @_;
	print "This is the in table: $inT\n";
}
sub count {
	$inT = shift @_;
	print "This is the in table: $inT\n";
}
sub listtables {
	foreach $table (keys %system) { print "$table\n"; }
}
sub quit { exit; }
sub fg { exit; }
sub exit { exit; }

sub help {

print " 
PROJECT <relation> OVER <attribute_list> GIVING <relation>.
SELECT <relation> WHERE <attribute><relation_operator><constant> GIVING <relation>.
DIFFERENCE <relation> FROM <relation> GIVING <relation>
JOIN <relation> AND <relation> OVER <attribute> GIVING <relation>
UNION <relation> AND <relation> GIVING <relation>.
INTERSECT <relation> AND <relation> GIVING <relation>
TIMES <relation> AND <relation> GIVING <relation>

<attribute_list> -> <attritute> {, <attribute> }
<attribute> -> any attribute name
<relational_operator> -> \"<\" | \"<=\" | \"=\" | \"!=\" | \">\" | \">=\"
<constant -> interger | string


SUM <attribute> IN <relation> 
LIST attribute IN <relation>
Print <relation>
Delete <relation>
COUNT <relation>
LIST relations
HELP shows this help
QUIT
EXIT

";
}

sub valid_table { return $system{$_[0]}; }
