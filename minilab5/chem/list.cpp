#include <iostream.h>
#include "list.h"

   list::list(int msize = 500) {
      work = head = foot = NULL;
      size = 0;
      maxsize = msize;
   }

   int list::insert(int *i) {
      if(isfull()) return size;
      work = new node(i);
      if (!foot) head = work;
      else {
         work->next = foot->next;
         work->prev = foot;
         foot->next = work;
      }
      foot = work;
      size++;
		return size;
   }

	int list::insert(node *new_node) {
		if(isfull()) return size; 
		if(!foot) head = new_node;
		else {
			new_node->next = foot->next;
			new_node->prev = foot;
			foot->next = new_node;
		}
		foot = new_node;
		size++;
		return size;
	}

	int *list::get_list(int list) {
		int counter;
		for(work = head; counter != list || work != NULL; work = work->next) counter++;
		if(work) return work->array;
		else return NULL;
	}

   node *list::delete_node(int *i) {
      if (head == NULL) return NULL;
      for (work = head; work != NULL; work = work->next) {
         if(work->array == i) {
            if(head == foot) head = foot = NULL;
            else if(work == foot) foot = foot->prev;
            else if(work == head) head = head->next;
            else {
               work->prev->next = work->next;
               work->next->prev = work->prev;
            }
            size--;
            return work;
         }
      }
      return NULL;
   }

   void list::display(void) {
      if (head == NULL) return;
      for (work = head; work != NULL; work = work->next) cout << work->value << " ";
   }

   list::~list(void) {
      if (head == NULL) return;
      else {
         work = head;
         while (work != NULL) {
            if (work->next != NULL) {
               work = work->next;
               delete work->prev;
            }
            else {
               delete work;
               work = head = foot = NULL;
            }
         }
      }
   }
