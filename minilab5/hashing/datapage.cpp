#include <iostream.h>
#include <math.h>
#include "datapage.h"

datapage::datapage(int depth=0, int max = 16) {
	work = head = foot = NULL;
	size = 0;
	//cout << "This is out max " << max << "\n";
	maxsize = max;
	page_depth = depth;
}

int datapage::insert(int i) {
      if(isfull()) return size;
      work = new node(i);
      if (!foot) head = work;
      else {
         work->next = foot->next;
         work->prev = foot;
         foot->next = work;
      }
      foot = work;
      size++;
      return size;
}

int datapage::insert(node *new_node) {
      if(isfull()) return size;
      if(!foot) {
			head = new_node;
		} else {

			//for(Node *temp = head; temp != NULL; temp = temp->next) {

			//}
         new_node->next = foot->next;
         new_node->prev = foot;
         foot->next = new_node;
      }
      	foot = new_node;
      size++;
      return size;
}

node *datapage::pop() {
	if (foot == NULL) return NULL;
	else {
		work = foot;
		foot = work->prev;
		if(foot) {
			foot->next = NULL;
		} else head = foot = NULL;
		return work;
	}
}

node *datapage::delete_node(int i) {
   if (head == NULL) return NULL;
   for (work = head; work != NULL; work = work->next) {
      if(work->value == i) {
         if(head == foot) head = foot = NULL;
         else if(work == foot) foot = foot->prev;
         else if(work == head) head = head->next;
         else {
            work->prev->next = work->next;
            work->next->prev = work->prev;
         }
         size--;
         return work;
      }
   }
   return NULL;
}

void datapage::display() {
	if (head == NULL) return;
	for (work = head; work != NULL; work = work->next) cout << "|" << work->value << "|\n";
}

int datapage::split_func(int i) {
 	return (int)floor((float)i / (float)pow(2, (17 - page_depth)));
}

datapage::~datapage() {
	// cout << "We are deleting this datapage\n";
   if (head != NULL) {
      work = head;
      while (work != NULL) {
         if (work->next != NULL) {
            work = work->next;
            delete work->prev;
         }
         else {
            delete work;
            work = head = foot = NULL;
         }
      }
   }
	//cout << "We are done deleting this datapage\n";
}
