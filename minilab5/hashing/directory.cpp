#include <iostream.h>
#include <math.h>
#include "directory.h"
#include "datapage.h"

directory::directory(void) {
	max_data_page_size = 16;
	page_array = new (datapage *)[2];
	page_array[0] = page_array[1] = new datapage(0);
   dir_depth = 1;
}

void directory::insert(int in) {
	// cout << "This is the value we are tring to insert " << in << "\n";
	int hash_val = hash_func(in);
	if(page_array[hash_val]->isfull()) {
		int page_depth = page_array[hash_val]->get_depth();
		if(page_depth == dir_depth) {
			double_dir();
			insert(in);
		} 
		else {
			datapage *old = page_array[hash_val];
			int i = hash_val;
			while(page_array[i] == page_array[i-1]) i--;
			// cout << "This is the page depth " << page_depth << "\n";
			// cout << "This is the dir depth " << dir_depth << "\n";
			int jump_pages =(int)(pow(2,((dir_depth - page_depth) - 1))); 
			// cout << "This is the number of jump pages " << jump_pages << "\n";
				datapage *new1 = new datapage((page_depth+1));	
				datapage *new2 = new datapage((page_depth+1));	
				for(int q = 0; q < jump_pages; q++) {
					page_array[i] = new1;
					page_array[i+jump_pages] = new2;
					i++;
				}
				while(node *fred = old->pop()) {
					insert(fred->value);
					delete fred;
				}
				delete old;
			insert(in);
		}
	}
	else {
		page_array[hash_val]->insert(in);
	}
}

void directory::double_dir() {
	dir_depth++;
	datapage **new_array = new (datapage *)[size()];
	for(int i = 0; i < (size()/2); i++) 
		new_array[(i*2)] = new_array[(i*2)+1] = page_array[i];
	delete page_array;
	page_array = new_array;
}

int directory::size() {
	return (int)pow((long)2,(long)dir_depth); 
}

void directory::display() {
	//cout << "--------------Starting---------------------\n";
	cout << "Directory depth is " << dir_depth << "\n";
	for(int i = 0; i < size(); i++) {
		//cout << "\nThis is directory Location " << i << " with local depth of " << page_array[i]->get_depth() << "\n";
		cout << "Directory: " << i << "\n";
		// cout << "This page depth is " << page_array[i]->get_depth() << "\n\n";
		page_array[i]->display();
	}
	// cout << "--------------Ending---------------------\n";
}

directory::~directory() {
	//cout << "We are deleting the objects\n";
	for(int i = 0; i < size(); i++) {
		//delete page_array[i];
	}
	//cout << "We are done deleting the objects\n";
}

int directory::hash_func(int i) {
 	// return (int)floor((float)i / (float)pow(2, (17 - dir_depth)));
	return (int)floor(((float)i / (float)100000) * (float)pow(2, dir_depth));
	// return ((i & 65535) % (int)pow(2, dir_depth));
}
