#include <iostream.h>
#include "datapage.h" 
#ifndef _directory_h_
#define _directory_h_
	class directory {
		public:
			directory(void);
			void insert(int data);
			void display();
			int hash_func(int i);
			int size();
			void double_dir();
			~directory(void);
		private:
		int max_data_page_size;
		datapage **page_array;
		int dir_depth;
	};
#endif
