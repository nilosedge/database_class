#include <iostream.h>
#ifndef _datapage_h_
#define _datapage_h_

class node {
  public:
      node(int i=0) { value = i; prev=next=NULL;}
      int value;
      node *next;
      node *prev;
}; 

class datapage {
		public:
			datapage(int depth=0, int max=16); 
			int insert(int i);
			int insert(node *in_node);
			void display();
			node *delete_node(int i);
			node *pop();
			int get_depth() { return page_depth; };
			// datapage *split();
			int split_func(int i);
			bool isfull() { return ((size == maxsize) ? true : false); };
			~datapage(void);
		private:
			int page_depth;
			datapage *next;
			node *work, *head, *foot;
			int size;
			int maxsize;
};
#endif
