#ifndef _list_h_
#define _list_h_

class node {
  public:
      node(int *fred=NULL) { array = fred; prev=next=NULL;}
      int *array;
      node *next;
      node *prev;
};

class list {
   public:
      list(int msize = 16);
      int insert(int i);
      int insert(node *new_node);
		int *get_list();
      bool isfull() { return ((size == maxsize) ? true : false);}
      node *delete_node(int i);
      void display(void);
      ~list(void);

   private:
      node *work, *head, *foot;
      int size;
      int maxsize;
};
#endif
